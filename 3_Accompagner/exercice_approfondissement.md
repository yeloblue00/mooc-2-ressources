# Enquête en ligne (et en anglais)

Cet exercice en ligne est proposé le Knight Lab de l'université américaine Northwerstern University.

- Le point de départ de l'histoire : un meurtre a été commis dans la ville de SQL City le 15 janvier 2018.

À partir de ce point de départ et d'une base de données dont le diagramme est donné ci-dessous, il s'agit de trouver le meurtrier.

![schema_relationnel](http://mystery.knightlab.com/schema.png)


Vous trouverez plus d'informations sur le dépôt GitHub du projet : https://github.com/NUKnightLab/sql-mysteries

Pour résoudre le meurtre, vous pouvez :
- le faire directement en ligne à l'adresse : http://mystery.knightlab.com/
- le faire en local (avec DB Browser for SQLite par exemple) en utilisant la base de données sql-murder-mystery.db (en la téléchargeant ici : https://github.com/NUKnightLab/sql-mysteries/raw/master/sql-murder-mystery.db)

Bonne enquête !
