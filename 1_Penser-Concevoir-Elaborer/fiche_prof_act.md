- **Thématique :** Base de données

- **Notions liées :** Langage SQL : requêtes d'interrogation d’une base de données.

- **Résumé de l’activité :** Exercices interactifs en langage SQL en ligne de manipulation des données de plusieurs tables.

- **Objectifs :** Construire des requêtes d'interrogation (SELECT, FROM, WHERE, JOIN)

- **Auteur :** FX Jollois

- **Durée de l’activité :** 1h

- **Forme de participation :** individuelle puis en binôme

- **Matériel nécessaire :** ordinateur connecté à internet

- **Préparation :** connaissance de la syntaxe avec des premiers exemples.

- **Autres références :** [Activité 1 : requêtage simple](https://fxjollois.github.io/cours-sql/)
