# Fiche d'intention / de mise en oeuvre de séance

## Etape 0

- Les élèves accèdent au site
- Vérifier qu'il n'y a de blocage techniques et les résoudre si besoin.

## Etape 1

- Travail de façon individuelle sur la première partie

-> passage dans les rangs pour controler l'avancement et les éventuels points de blocages.

## Etape 2

- Travail en binome si besoin pour faire progresser les réflexions

-> animation de l'enseignant pour les échanges

## Etape 3

Synthèse collaborative via une mise en commun des solutions et des difficultés rencontrées.

-> rôle d'animateur de l'enseignant
